using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Unity;

public class BottomBarUIController : MonoBehaviour
{
    public bool enableDebug = false;
    public GameObject BottomBar;
    public GameObject DetailView;

    public string k_textFieldStartingText = "Looking for Objects";

    // Start is called before the first frame update
    void Start()
    {
        BottomBar.GetComponentInChildren<Text>().text = k_textFieldStartingText;
    }

    public void Debug(string text)
    {
        if (enableDebug)
        {
            BottomBar.GetComponentInChildren<Text>().text = text;
        }
    }

    public void ShowInfoForGUID(string guid)
    {
        HolzContentEntry entry = HolzContent.entries.First(item => item.guid.Equals(guid));
        BottomBar.SetActive(false);
        DetailView.SetActive(true);
        DetailView.transform.Find("TopBar").GetComponentInChildren<Text>().text = entry.itemName;
        DetailView.transform.Find("Scroll View").GetComponentInChildren<Text>().text = entry.description;
    }

    public void HideInfo()
    {
        BottomBar.SetActive(true);
        DetailView.SetActive(false);
        BottomBar.GetComponentInChildren<Text>().text = k_textFieldStartingText;
    }
}
