using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation.Samples;

public class ObjectController : MonoBehaviour
{

    EventSystem m_eventSystem;
    BottomBarUIController m_bottomBar;
    PrefabImagePairManager m_PrefabImageManager;

    void OnEnable()
    {
        m_eventSystem = EventSystem.current;
        m_bottomBar = GetComponent<BottomBarUIController>();
        m_PrefabImageManager = GetComponent<PrefabImagePairManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        m_eventSystem.currentSelectedGameObject.transform.Rotate(new Vector3(0f, 0.3f, 0f), Space.Self);
    }

    private void Update()
    {
        if (Input.touchCount > 0 && Input.touchCount < 2)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null || true)
                {
                    GameObject targetObject = hit.collider.gameObject;
                    m_eventSystem.SetSelectedGameObject(targetObject);
                    m_bottomBar.Debug(m_PrefabImageManager.GetGUIDForObject(targetObject));
                    SelectedGameObject(targetObject);
                }
            }
        }
        else if (Input.touchCount == 2)
        {
            m_bottomBar.HideInfo();
        }
    }

    private void SelectedGameObject(GameObject selected)
    {
        m_bottomBar.Debug(m_PrefabImageManager.GetGUIDForObject(selected));
        if (selected)
        {
            m_bottomBar.ShowInfoForGUID(m_PrefabImageManager.GetGUIDForObject(selected));
        } else
        {
            m_bottomBar.HideInfo();
        }
    }
}
