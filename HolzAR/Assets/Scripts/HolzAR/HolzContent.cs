using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolzContent
{
    public static HolzContentEntry[] entries =
        {
        new HolzContentEntry("Beech", "2c29eeed-d95c-4111-b724-80f9ff10ce86", @"Beech (Fagus) is a genus of deciduous trees in the family Fagaceae, native to temperate Europe, Asia, and North America. Recent classifications recognize 10 to 13 species in two distinct subgenera, Engleriana and Fagus. The Engleriana subgenus is found only in East Asia, distinctive for their low branches, often made up of several major trunks with yellowish bark. The better known Fagus subgenus beeches are high-branching with tall, stout trunks and smooth silver-grey bark. The European beech (Fagus sylvatica) is the most commonly cultivated.

Beeches are monoecious, bearing both male and female flowers on the same plant. The small flowers are unisexual, the female flowers borne in pairs, the male flowers wind-pollinating catkins. They are produced in spring shortly after the new leaves appear. The fruit of the beech tree, known as beechnuts or mast, is found in small burrs that drop from the tree in autumn. They are small, roughly triangular and edible, with a bitter, astringent, or mild and nut-like taste.

The European species Fagus sylvatica yields a utility timber that is tough but dimensionally unstable. It is widely used for furniture framing and carcase construction, flooring and engineering purposes, in plywood and in household items like plates, but rarely as a decorative wood. The timber can be used to build chalets, houses, and log cabins.

Beech wood also makes excellent firewood, easily split and burning for many hours with bright but calm flames. Slats of washed beech wood are spread around the bottom of fermentation tanks for Budweiser (Anheuser-Busch) beer. Beech logs are burned to dry the malt used in some German smoked beers. Beech is also used to smoke Westphalian ham, andouille sausage, and some cheeses.
"),
        new HolzContentEntry("Walnut", "aae75228-1973-4dc0-b7a2-53d1566ee402", @"The common walnut, and the black walnut and its allies, are important for their attractive timber, which is hard, dense, tight-grained and polishes to a very smooth finish. The color is dark chocolate or similar in the heartwood changing by a sharp boundary to creamy white in the sapwood. When kiln-dried, walnut wood tends toward a dull brown color, but when air-dried can become a rich purplish-brown. Because of its color, hardness and grain, it is a prized furniture and carving wood. Walnut burls (or ""burrs"" in Europe) are commonly used to create bowls and other turned pieces. The grain figure exposed when a crotch (fork) in a walnut log is cut in the plane of its one entering branch and two exiting branches is attractive and sought after. Veneer sliced from walnut burl is one of the most valuable and highly prized by cabinet makers and prestige car manufacturers. Walnut wood has been the timber of choice for gun makers for centuries, including the Gewehr 98 and Lee–Enfield rifles of the First World War. It remains one of the most popular choices for rifle and shotgun stocks, and is generally considered to be the premium – as well as the most traditional – wood for gun stocks, due to its resilience to compression along the grain. Walnut is also used in lutherie and for the body of pipe organs. The wood of the butternut and related Asian species is of much lower value, softer, coarser, less strong and heavy, and paler in colour."),
        new HolzContentEntry("Oak", "f0340be5-710e-49ff-bc70-45f1315b9202", @"An oak is a tree or shrub in the genus Quercus (/ˈkwɜːrkəs/;[1] Latin ""oak tree"") of the beech family, Fagaceae. There are approximately 500 extant species of oaks.[2] The common name ""oak"" also appears in the names of species in related genera, notably Lithocarpus (stone oaks), as well as in those of unrelated species such as Grevillea robusta (silky oaks) and the Casuarinaceae (she-oaks). The genus Quercus is native to the Northern Hemisphere, and includes deciduous and evergreen species extending from cool temperate to tropical latitudes in the Americas, Asia, Europe, and North Africa. North America contains the largest number of oak species, with approximately 90 occurring in the United States, while Mexico has 160 species of which 109 are endemic. The second greatest center of oak diversity is China, which contains approximately 100 species.[3]


Solitary oak, the Netherlands

Oak: male flowers

The leaves of a young oak
Oaks have spirally arranged leaves, with lobate margins in many species; some have serrated leaves or entire leaves with smooth margins.Many deciduous species are marcescent, not dropping dead leaves until spring. In spring, a single oak tree produces both male flowers (in the form of catkins) and small female flowers,[4] meaning that the trees are monoecious.The fruit is a nut called an acorn or oak nut borne in a cup-like structure known as a cupule; each acorn contains one seed(rarely two or three) and takes 6–18 months to mature, depending on their species.The acorns and leaves contain tannic acid, [5] which helps to guard from fungi and insects.[6] The live oaks are distinguished for being evergreen, but are not actually a distinct group and instead are dispersed across the genus."),
    };
}

public struct HolzContentEntry
{
    public string itemName;
    public string guid;
    public string description;

    public HolzContentEntry(string itemName, string guid, string description)
    {
        this.itemName = itemName;
        this.guid = guid;
        this.description = description;
    }
}