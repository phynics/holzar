using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMessageBoxManager : MonoBehaviour
{

    public Text outputTextBox;
    private List<QueuedMessage> messageQueue;

    private static UIMessageBoxManager _instance;

    public static UIMessageBoxManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UIMessageBoxManager>();
            }

            return _instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }


    // Start is called before the first frame update
    void Start()
    {
        messageQueue = new List<QueuedMessage>();
        StartCoroutine(CheckQueue());

        QueueInMessage("Looking for floor...", 10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetNewMessage()
    {
        if(messageQueue.Count > 0)
        {
            outputTextBox.text = messageQueue[0].message;
        } else
        {
            //gameObject.SetActive(false);
        }
    }

    public void QueueInMessage(string message, int time)
    {
        QueuedMessage newMessage = new QueuedMessage();
        newMessage.message = message;
        newMessage.time = time;

        messageQueue.Add(newMessage);

        if(messageQueue.Count == 1)
        {
            SetNewMessage();
        }

    }

    public void ClearQueue()
    {
        messageQueue.Clear();
    }

    public void ClearAndMessage(string message, int time)
    {
        ClearQueue();
        QueueInMessage(message, time);
    }

    IEnumerator CheckQueue()
    {
        while(true)
        {
            if (messageQueue.Count == 0)
            {
                //gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(true);
            }

            if (messageQueue.Count > 0)
            {
                messageQueue[0].time -= 1;
                if (messageQueue[0].time < 1)
                {
                    messageQueue.RemoveAt(0);
                    SetNewMessage();
                }
            }

            yield return new WaitForSeconds(1);
        }
    }
}

class QueuedMessage
{
    public string message;
    public int time;
}