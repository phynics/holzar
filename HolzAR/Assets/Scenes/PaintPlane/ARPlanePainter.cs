using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARPlanePainter : MonoBehaviour
{

    private int pos = 0;

    public Material[] mats;
    public Material[] weatheredMats;

    bool isShowingWeathered = false;

    public MeshRenderer currentPlane;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextPlane()
    {
        pos += 1;
        if(pos >= mats.Length)
        {
            pos = 0;
        }

        ChangeTextures(pos);
    }

    public void SwitchWeathered()
    {
        if(isShowingWeathered)
        {
            isShowingWeathered = false;
            ChangeTextures(pos);
        } else
        {
            isShowingWeathered = true;
            ChangeTextures(pos);
        }
    }

    private void ChangeTextures(int toIndex)
    {
        Material material = isShowingWeathered ? weatheredMats[pos] : mats[pos];
        currentPlane.material = material;
        UIMessageBoxManager.Instance.QueueInMessage(material.name, 3);
        var renderers = gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach(var renderer in renderers)
        {
            renderer.material = material;
        }
    }
}
